export const NotificationData = [
  {
    id: 1,
    title: 'You got a tweet',
    date: '2021/01/22',
  },
  {
    id: 2,
    title: 'You got New email',
    date: '2011/01/03',
  },
  {
    id: 3,
    title: 'Shoe stock running out',
    date: '2021/11/22',
  },
  {
    id: 4,
    title: 'You got a tweet',
    date: '2021/01/22',
  },
  {
    id: 5,
    title: 'You got New email',
    date: '2011/01/03',
  },
  {
    id: 6,
    title: 'Shoe stock running out',
    date: '2021/11/22',
  },
  {
    id: 7,
    title: 'You got a tweet',
    date: '2021/01/22',
  },
  {
    id: 8,
    title: 'You got New email',
    date: '2011/01/03',
  },
  {
    id: 9,
    title: 'Shoe stock running out',
    date: '2021/11/22',
  },
];
