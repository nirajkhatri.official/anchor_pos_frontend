import ShoppingCartIcon from '@mui/icons-material/ShoppingCart';
import BorderColorIcon from '@mui/icons-material/BorderColor';
import LoyaltyIcon from '@mui/icons-material/Loyalty';
import BadgeIcon from '@mui/icons-material/Badge';
import EmojiPeopleIcon from '@mui/icons-material/EmojiPeople';
import CardGiftcardIcon from '@mui/icons-material/CardGiftcard';
import SettingsIcon from '@mui/icons-material/Settings';

export const MainMenuData = [
  {
    type: 'dropdown',
    name: 'Products',
    icon: ShoppingCartIcon,
    link: '/product',
    data: [
      {
        name: 'Add Product',
        icon: ShoppingCartIcon,
        link: '/addproduct',
      },
      {
        name: 'Product Detail',
        icon: ShoppingCartIcon,
        link: '/productdetail',
      },
    ],
  },
  {
    type: 'normal',
    name: 'Orders',
    icon: BorderColorIcon,
    link: '/order',
  },
  {
    type: 'normal',
    name: 'Sales',
    icon: LoyaltyIcon,
    link: '/sale',
  },
  {
    type: 'normal',
    name: 'Employees',
    icon: BadgeIcon,
    link: '/employee',
  },
  {
    type: 'normal',
    name: 'Customers',
    icon: EmojiPeopleIcon,
    link: '/customer',
  },
  {
    type: 'normal',
    name: 'Gift Cards',
    icon: CardGiftcardIcon,
    link: '/giftcard',
  },
  {
    type: 'dropdown',
    name: 'Security Settings',
    icon: SettingsIcon,
    data: [
      {
        name: 'Security Add',
        icon: ShoppingCartIcon,
      },
      {
        name: 'Security Removeeee',
        icon: ShoppingCartIcon,
      },
    ],
  },
];
