import HomeIcon from '@mui/icons-material/Home';
import ShowChartIcon from '@mui/icons-material/ShowChart';
import CategoryIcon from '@mui/icons-material/Category';
import ConstructionIcon from '@mui/icons-material/Construction';
import BrandingWatermarkIcon from '@mui/icons-material/BrandingWatermark';
import ReportIcon from '@mui/icons-material/Report';
export const GeneralMenuData = [
  { type: 'normal', name: 'Dashboard', icon: HomeIcon, link: '/' },
  {
    type: 'dropdown',
    name: 'Setup Settings',
    icon: ShowChartIcon,
    data: [
      {
        name: 'Category',
        icon: CategoryIcon,
        link: '/category',
      },
      {
        name: 'Vendor',
        icon: ConstructionIcon,
        link: '/vendor',
      },
      {
        name: 'Brand',
        icon: BrandingWatermarkIcon,
        link: '/brand',
      },
    ],
  },
  {
    type: 'normal',
    name: 'Reports',
    icon: ReportIcon,
    link: '/report',
  },
];
