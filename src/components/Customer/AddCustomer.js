import {
  Button,
  Grid,
  IconButton,
  Paper,
  TextField,
  Typography,
} from '@mui/material';
import React, { useState } from 'react';
import { Box } from '@mui/system';
import CloudUploadIcon from '@mui/icons-material/CloudUpload';
import unknown_profile from '../../Images/unknown_profile.png';
import AddCircleRoundedIcon from '@mui/icons-material/AddCircleRounded';
import AddIcon from '../../utils/AddIcon';

const Customer = () => {
  const [image, setImage] = useState('');
  const [uploaded, isUploaded] = useState(false);
  const handleImageChange = e => {
    if (e.target.files && e.target.files[0]) {
      let reader = new FileReader();

      reader.onload = function (e) {
        setImage(e.target.result);
        isUploaded(true);
      };
      reader.readAsDataURL(e.target.files[0]);
    }
  };

  return (
    <>
      <Paper elevation={5}>
        <AddIcon title='Add Customer' />
        <Grid sx={{ padding: 3 }} container>
          <Grid
            sx={{
              display: 'flex',
              flexDirection: 'column',
              gap: 2,
            }}
            item
            xs={5.5}
          >
            <TextField color='secondary' label='Customer Name' />
            <TextField color='secondary' label='Customer Contact' />
          </Grid>
          <Grid item xs={1} />
          <Grid
            item
            sx={{ display: 'flex', flexDirection: 'column', gap: 2 }}
            xs={5.5}
          >
            <TextField fullWidth color='secondary' label='Customer Name' />
            <Box
              sx={{
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'flex-end',
                gap: 2,
                height: '100%',
              }}
            >
              <Button variant='contained' size='large' color='error'>
                Clear
              </Button>
              <Button variant='outlined' size='large' color='success'>
                Submit
              </Button>
            </Box>
          </Grid>
        </Grid>
      </Paper>
    </>
  );
};

export default Customer;
