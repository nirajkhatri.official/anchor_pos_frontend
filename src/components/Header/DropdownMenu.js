import ExpandMore from '@mui/icons-material/ExpandMore';
import {
  Accordion,
  AccordionDetails,
  AccordionSummary,
  List,
  ListItem,
  ListItemButton,
  ListItemIcon,
  ListItemText,
  Typography,
} from '@mui/material';
import { Box } from '@mui/system';

import React from 'react';
import { useNavigate } from 'react-router';

const DropdownMenu = ({ el }) => {
  const navigate = useNavigate();
  return (
    <Box>
      <Accordion
        sx={{
          boxShadow: 'none',
          border: 'none',
          backgroundColor: 'secondary.accordian',
        }}
      >
        <AccordionSummary
          sx={{ backgroundColor: 'transparent' }}
          expandIcon={<ExpandMore />}
        >
          <ListItemIcon>
            <el.icon color='secondary' sx={{ ml: 1 }} />
          </ListItemIcon>
          <Typography>{el.name}</Typography>
        </AccordionSummary>
        <AccordionDetails>
          <List
            sx={{
              display: 'flex',
              flexDirection: 'column',
              alignItems: 'flex-start',
            }}
            disablePadding
          >
            {el.data.map(el => (
              <ListItem button onClick={() => navigate(el.link)}>
                <ListItemIcon>
                  <el.icon color='secondary' sx={{ ml: 1 }} />
                </ListItemIcon>
                <ListItemText primary={el.name} />
              </ListItem>
            ))}
          </List>
        </AccordionDetails>
      </Accordion>
    </Box>
  );
};

export default DropdownMenu;
