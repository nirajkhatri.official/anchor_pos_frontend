import {
  Avatar,
  Button,
  Divider,
  IconButton,
  InputBase,
  Menu,
  MenuItem,
  Toolbar,
  Box,
  Typography,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  Card,
  CardContent,
  CardActions,
  Tooltip,
  Accordion,
  AccordionSummary,
  AccordionDetails,
} from '@mui/material';
import MuiDrawer from '@mui/material/Drawer';
import MuiAppBar from '@mui/material/AppBar';
import React, { useContext, useState } from 'react';
import SearchIcon from '@mui/icons-material/Search';
import { styled } from '@mui/material/styles';
import NotificationsIcon from '@mui/icons-material/Notifications';
import KeyboardArrowDown from '@mui/icons-material/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@mui/icons-material/KeyboardArrowUp';
import HomeIcon from '@mui/icons-material/Home';
import ShowChartIcon from '@mui/icons-material/ShowChart';
import PaidIcon from '@mui/icons-material/Paid';
import TelegramIcon from '@mui/icons-material/Telegram';
import PriceCheckIcon from '@mui/icons-material/PriceCheck';
import AccountBalanceWalletIcon from '@mui/icons-material/AccountBalanceWallet';
import SettingsIcon from '@mui/icons-material/Settings';
import LogoutIcon from '@mui/icons-material/Logout';
import ArrowCircleLeftOutlinedIcon from '@mui/icons-material/ArrowCircleLeftOutlined';
import ArrowCircleRightOutlinedIcon from '@mui/icons-material/ArrowCircleRightOutlined';
import HelpOutlineOutlinedIcon from '@mui/icons-material/HelpOutlineOutlined';
import LightModeIcon from '@mui/icons-material/LightMode';
import DarkModeIcon from '@mui/icons-material/DarkMode';
import { ColorModeContext } from '../../index';
import StarBorder from '@mui/icons-material/StarBorder';
import ListItemButton from '@mui/material/ListItemButton';
import Collapse from '@mui/material/Collapse';
import ExpandLess from '@mui/icons-material/ExpandLess';
import ExpandMore from '@mui/icons-material/ExpandMore';
import { useNavigate } from 'react-router';
import CategoryIcon from '@mui/icons-material/Category';
import BrandingWatermarkIcon from '@mui/icons-material/BrandingWatermark';
import ConstructionIcon from '@mui/icons-material/Construction';
import ReportIcon from '@mui/icons-material/Report';
import ShoppingCartIcon from '@mui/icons-material/ShoppingCart';
import BorderColorIcon from '@mui/icons-material/BorderColor';
import LoyaltyIcon from '@mui/icons-material/Loyalty';
import BadgeIcon from '@mui/icons-material/Badge';
import EmojiPeopleIcon from '@mui/icons-material/EmojiPeople';
import CardGiftcardIcon from '@mui/icons-material/CardGiftcard';
import PasswordIcon from '@mui/icons-material/Password';
import DropdownMenu from './DropdownMenu';
import SideMenuItem from './SideMenuItem';
import { GeneralMenuData } from '../../data/GeneralMenuData';
import { MainMenuData } from '../../data/MainMenuData';
import NoteAddIcon from '@mui/icons-material/NoteAdd';
const drawerWidth = 240;

const openedMixin = theme => ({
  width: drawerWidth,
  transition: theme.transitions.create('width', {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.enteringScreen,
  }),
  overflowX: 'hidden',
});

const closedMixin = theme => ({
  transition: theme.transitions.create('width', {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.leavingScreen,
  }),
  overflowX: 'visible',
  overflowY: 'visible',
  width: `calc(${theme.spacing(7)} + 1px)`,
  [theme.breakpoints.up('sm')]: {
    width: `calc(${theme.spacing(9)} + 1px)`,
  },
});

const DrawerHeader = styled('div')(({ theme }) => ({
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'flex-end',
  padding: theme.spacing(0, 1),
  ...theme.mixins.toolbar,
}));

const AppBar = styled(MuiAppBar, {
  shouldForwardProp: prop => prop !== 'open',
})(({ theme, open }) => ({
  zIndex: theme.zIndex.drawer + 1,
  transition: theme.transitions.create(['width', 'margin'], {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.leavingScreen,
  }),
  ...(open && {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  }),
}));

const Drawer = styled(MuiDrawer, {
  shouldForwardProp: prop => prop !== 'open',
})(({ theme, open }) => ({
  width: drawerWidth,
  flexShrink: 0,
  whiteSpace: 'nowrap',
  boxSizing: 'border-box',
  ...(open && {
    ...openedMixin(theme),
    '& .MuiDrawer-paper': openedMixin(theme),
  }),
  ...(!open && {
    ...closedMixin(theme),
    '& .MuiDrawer-paper': closedMixin(theme),
  }),
}));

const Search = styled('div')(({ theme }) => ({
  position: 'relative',
  borderRadius: theme.shape.borderRadius,
  backgroundColor: '#E0E0E0',
  '&:hover': {
    backgroundColor: '#E0E0E0',
  },
  marginLeft: 0,
  width: '100%',
  [theme.breakpoints.up('sm')]: {
    marginLeft: theme.spacing(1),
    width: 'auto',
  },
}));

const SearchIconWrapper = styled('div')(({ theme }) => ({
  padding: theme.spacing(0, 2),
  height: '100%',
  color: 'black',
  position: 'absolute',
  pointerEvents: 'none',
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
}));

const StyledInputBase = styled(InputBase)(({ theme }) => ({
  color: 'black',
  '& .MuiInputBase-input': {
    padding: theme.spacing(1, 1, 1, 0),
    paddingLeft: `calc(1em + ${theme.spacing(4)})`,
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      width: '12ch',
      '&:focus': {
        width: '20ch',
      },
    },
  },
}));

const Header = ({ children }) => {
  const { toggleColorMode } = useContext(ColorModeContext);
  const navigate = useNavigate();
  const [profileMenu, setProfileMenu] = useState(null);
  const [notificationMenu, setnotificationMenu] = useState(null);
  const [openDrawer, setOpenDrawer] = useState(false);
  const [dark, setDark] = useState(false);
  const [dropdownGeneral, setDropdownGeneral] = useState(false);
  const [dropdownMain, setDropdownMain] = useState(false);
  const [productMenu, setProductMenu] = useState(false);
  const open = Boolean(profileMenu);
  const notification = Boolean(notificationMenu);
  const [tooltip, setTooltip] = useState({ display: 'none' });
  const profileClickHandler = e => {
    setProfileMenu(e.currentTarget);
  };
  const handleProfileMenuClose = () => {
    setProfileMenu(null);
  };

  const handleDrawerClose = () => {
    setOpenDrawer(false);
  };
  const handleToggleDrawer = () => setOpenDrawer(!openDrawer);
  const handleDarkMode = () => {
    toggleColorMode();
    handleProfileMenuClose();
    setDark(!dark);
  };
  const notificationClickHandler = e => {
    setnotificationMenu(e.currentTarget);
  };
  const handleNotificationClose = () => {
    setnotificationMenu(null);
  };
  const notificationRedirect = () => {
    handleNotificationClose();
    navigate('/notification');
  };
  return (
    <>
      {/* Top navigation bar starts here */}
      <AppBar position='sticky' open={openDrawer}>
        <Toolbar>
          {openDrawer ? (
            <IconButton
              sx={{
                position: 'absolute',
                left: -20,
                ':hover': { backgroundColor: 'secondary.main' },
              }}
            >
              <ArrowCircleLeftOutlinedIcon
                sx={{
                  color: 'black',
                  backgroundColor: 'white',
                  borderRadius: 50,
                }}
                onClick={handleToggleDrawer}
              />
            </IconButton>
          ) : (
            <IconButton
              sx={{
                mr: 2,
                ml: -1,
                ':hover': { backgroundColor: 'secondary.main' },
              }}
            >
              <ArrowCircleRightOutlinedIcon
                sx={{
                  color: 'black',
                  backgroundColor: 'white',
                  borderRadius: 50,
                }}
                onClick={handleToggleDrawer}
              />
            </IconButton>
          )}

          <Typography sx={{ flexGrow: 1 }}>Anchor Digital Agency</Typography>
          <Search>
            <SearchIconWrapper>
              <SearchIcon />
            </SearchIconWrapper>
            <StyledInputBase placeholder='Search...' />
          </Search>
          <Divider
            sx={{ height: '20px', ml: 2, mr: 2, backgroundColor: 'black' }}
            orientation='vertical'
          />
          <Box sx={{ display: 'flex', alignItems: 'center' }}>
            <IconButton>
              <NoteAddIcon
                color='secondary'
                onClick={() => navigate('/note')}
              />
            </IconButton>
            <IconButton>
              <NotificationsIcon
                onClick={notificationClickHandler}
                color='secondary'
              />
              <Menu
                id='demo-positioned-menu'
                aria-labelledby='demo-positioned-button'
                anchorEl={notificationMenu}
                open={notificationMenu}
                onClose={handleNotificationClose}
                sx={{ minWidth: '250px', maxWidth: '250px', mt: 2.5, ml: -4 }}
              >
                <MenuItem
                  sx={{
                    whiteSpace: 'normal',
                    minWidth: '250px',
                    maxWidth: '250px',
                  }}
                  onClick={notificationRedirect}
                >
                  You got tweet
                </MenuItem>
                <Divider />
                <MenuItem
                  sx={{ whiteSpace: 'normal' }}
                  onClick={notificationRedirect}
                >
                  <Typography nowrap> You got an leave notification</Typography>
                </MenuItem>
                <Divider />
                <MenuItem onClick={notificationRedirect}>Logout</MenuItem>
                <Divider />
                <Button
                  sx={{ width: '100%', mt: -1, mb: -1 }}
                  color='info'
                  variant='contained'
                  onClick={notificationRedirect}
                >
                  See All Notification
                </Button>
              </Menu>
            </IconButton>
            <IconButton>
              <Avatar
                alt='profile picture'
                sx={{ height: 30, width: 30 }}
                src='https://picsum.photos/200/300'
              />
            </IconButton>
            <Box>
              <Button
                id='profile-button'
                aria-controls='profile-menu'
                aria-haspopup='true'
                onClick={profileClickHandler}
                sx={{
                  ':focus': { backgroundColor: 'main' },
                  ':hover': { backgroundColor: 'main' },
                }}
                disableRipple
                variant='primary'
              >
                Niraj Khatri
                {open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDown />}
              </Button>

              <Menu
                id='profile-menu'
                anchorEl={profileMenu}
                open={open}
                onClose={handleProfileMenuClose}
              >
                <MenuItem
                  sx={{ mt: -1, mb: -1, justifyContent: 'center' }}
                  onClick={handleDarkMode}
                >
                  {dark ? <LightModeIcon /> : <DarkModeIcon />}
                </MenuItem>

                <Divider />
                <MenuItem
                  sx={{ mt: -1, mb: -1 }}
                  onClick={handleProfileMenuClose}
                >
                  Profile
                </MenuItem>
                <Divider />
                <MenuItem
                  sx={{ mt: -1, mb: -1 }}
                  onClick={handleProfileMenuClose}
                >
                  My Account
                </MenuItem>
                <Divider />
                <MenuItem
                  sx={{ mt: -1, mb: -1 }}
                  onClick={handleProfileMenuClose}
                >
                  Logout
                </MenuItem>
              </Menu>
            </Box>
          </Box>
        </Toolbar>
      </AppBar>
      {/* Top navigation bar ends here */}
      {/* Side  navigation bar starts here */}
      <Box sx={{ display: 'flex' }}>
        <Drawer open={openDrawer} variant='permanent'>
          <DrawerHeader
            sx={{
              display: 'flex',
              flexDirection: 'column',
              justifyContent: 'center',
              padding: '20px',
              minHeight: '85px !important',
            }}
          >
            <Typography>Anchor Digital Agency</Typography>
            <Divider sx={{ width: '100px', backgroundColor: 'white' }} />
            <Typography variant='body1'>Admin</Typography>
          </DrawerHeader>
          <Divider
            sx={{
              width: '80%',
              ml: 'auto',
              mr: 'auto',
              mb: 2,
              backgroundColor: 'white',
            }}
          />
          {openDrawer && (
            <Box
              sx={{
                display: 'flex',
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-evenly',
              }}
            >
              <Typography>General Menu</Typography>
              <Divider
                sx={{
                  width: '40%',

                  backgroundColor: 'white',
                }}
              />
            </Box>
          )}
          <List sx={{ display: 'flex', flexDirection: 'column' }}>
            {GeneralMenuData.map((el, index) => (
              <>
                {openDrawer ? (
                  el.type == 'normal' ? (
                    <SideMenuItem el={el} />
                  ) : (
                    <DropdownMenu el={el} />
                  )
                ) : (
                  <ListItem
                    sx={{ width: '89%' }}
                    onClick={handleDrawerClose}
                    button
                    key={el.name}
                  >
                    <ListItemIcon
                      onMouseEnter={() =>
                        setTooltip({
                          display: 'block',
                          position: 'absolute',
                          zIndex: 'tooltip',
                          color: 'white',
                          backgroundColor: 'black',
                          width: '120px',
                          borderRadius: 10,
                          left: 72,
                        })
                      }
                      onMouseLeave={() => setTooltip({ display: 'none' })}
                    >
                      <el.icon color='secondary' sx={{ ml: 1 }} />

                      <Box style={tooltip}>
                        <Typography textAlign='center'>{el.name}</Typography>
                      </Box>
                    </ListItemIcon>
                  </ListItem>
                )}
              </>
            ))}
          </List>
          {openDrawer && (
            <Box
              sx={{
                display: 'flex',
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-evenly',
              }}
            >
              <Typography>Main Menu</Typography>
              <Divider
                sx={{
                  width: '40%',
                  backgroundColor: 'white',
                }}
              />
            </Box>
          )}
          <List sx={{ display: 'flex', flexDirection: 'column' }}>
            {MainMenuData.map((el, index) => (
              <>
                {openDrawer ? (
                  el.type == 'normal' ? (
                    <SideMenuItem el={el} />
                  ) : (
                    <DropdownMenu el={el} />
                  )
                ) : (
                  <Tooltip
                    arrow
                    title={
                      <h3
                        style={{
                          padding: 8,
                          fontSize: 16,
                          fontWeight: 500,
                          letterSpacing: 1.1,
                          margin: '-4px -8px',
                          borderRadius: 2,
                        }}
                      >
                        {el.name}
                      </h3>
                    }
                    placement='right'
                  >
                    <ListItem
                      sx={{ width: '89%' }}
                      onClick={handleDrawerClose}
                      button
                      key={el.name}
                    >
                      <ListItemIcon>
                        <el.icon color='secondary' sx={{ ml: 1 }} />
                      </ListItemIcon>
                    </ListItem>
                  </Tooltip>
                )}
              </>
            ))}
            <Divider
              sx={{
                width: '80%',
                ml: 'auto',
                mr: 'auto',
                mt: 2,
              }}
            />
            {/* Side navigation bar help section starts here */}
            {openDrawer && (
              <IconButton>
                <HelpOutlineOutlinedIcon
                  sx={{
                    color: 'green',
                    backgroundColor: 'white',
                    borderRadius: 50,
                    position: 'absolute',
                    top: '50%',
                  }}
                />
              </IconButton>
            )}

            {openDrawer && (
              <Card
                sx={{
                  mr: 1,
                  ml: 1,
                  display: 'flex',
                  flexDirection: 'column',
                  justifyContent: 'center',
                  alignItems: 'center',
                  backgroundColor: 'secondary.main',
                }}
              >
                <CardContent>
                  <Typography
                    color='primary.main'
                    sx={{ mt: 1 }}
                    variant='body2'
                  >
                    Need Help With GRC Dashboard?
                  </Typography>
                </CardContent>
                <CardActions>
                  <Button size='small' color='success' variant='contained'>
                    Go to help center
                  </Button>
                </CardActions>
              </Card>
            )}
            {/* Side navigation bar help section ends here */}
          </List>
        </Drawer>
        {/* Side navigation bar ends here */}
        <Card
          sx={{
            height: '90vh',
            flex: 1,
            margin: '10px',
            padding: '15px',
            overflow: 'auto',
          }}
        >
          {children}
        </Card>
      </Box>
    </>
  );
};

export default Header;
