import { ListItem, ListItemIcon, ListItemText } from '@mui/material';
import React from 'react';
import { useNavigate } from 'react-router';

const SideMenuItem = ({ el }) => {
  const navigate = useNavigate();
  return (
    <ListItem onClick={() => navigate(el.link)} button key={el.name}>
      <ListItemIcon>
        <el.icon color='secondary' sx={{ ml: 1 }} />
      </ListItemIcon>

      <ListItemText primary={el.name} />
    </ListItem>
  );
};

export default SideMenuItem;
