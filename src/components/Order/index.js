import React, { useState } from 'react';
import { Box } from '@mui/system';
import { Button, MenuItem, TextField } from '@mui/material';
import { ProductData } from './../../data/ProductData';

const Order = () => {
  const [addOrder, setAddOrder] = useState([]);

  const handleAddOrder = () => {
    setAddOrder(prev => [...prev, { product_name: '', productDetail: [] }]);
  };

  const handleSelectProduct = (index, e) => {
    const product_detail = ProductData.filter(
      el => el.product_name === e.target.value
    );

    const values = [...addOrder];
    values[index][e.target.name] = e.target.value;
    setAddOrder(values);
  };

  return (
    <Box>
      <Button onClick={handleAddOrder} variant='outlined' color='secondary'>
        Add Order
      </Button>
      {console.log(addOrder)}
      {addOrder.map((order, index) => {
        return (
          <Box sx={{ mt: 2 }} key={index}>
            <TextField
              color='secondary'
              select
              name='product_name'
              label='Product Name'
              value={order.product_name}
              sx={{ width: 200 }}
              onChange={e => handleSelectProduct(index, e)}
            >
              {ProductData.map(el => (
                <MenuItem key={el.product_name} value={el.product_name}>
                  {el.product_name}
                </MenuItem>
              ))}
            </TextField>
          </Box>
        );
      })}
    </Box>
  );
};

export default Order;
