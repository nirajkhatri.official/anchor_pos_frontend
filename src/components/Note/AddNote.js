import React, { useEffect, useState } from 'react';
import { Editor } from 'react-draft-wysiwyg';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import { Box } from '@mui/system';
import { EditorState } from 'draft-js';

const AddNote = () => {
  const [editorState, setEditorState] = useState(() =>
    EditorState.createEmpty()
  );
  const handleEditorState = editorState => {
    setEditorState(editorState);
  };
  return (
    <Box>
      <Editor
        editorState={editorState}
        onEditorStateChange={handleEditorState}
      />
    </Box>
  );
};

export default AddNote;
