import {
  Autocomplete,
  Box,
  Button,
  Card,
  CardActions,
  CardContent,
  CardMedia,
  Divider,
  FormControl,
  Grid,
  IconButton,
  InputLabel,
  MenuItem,
  Modal,
  Paper,
  Select,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TablePagination,
  TableRow,
  TextField,
  Typography,
} from '@mui/material';
import React, { useState } from 'react';
import { ProductData } from '../../data/ProductData';
import EditIcon from '@mui/icons-material/Edit';
import DeleteIcon from '@mui/icons-material/Delete';
import InfoIcon from '@mui/icons-material/Info';
import CloseIcon from '@mui/icons-material/Close';
import unknown_profile from '../../Images/unknown_profile.png';
import CloudUploadIcon from '@mui/icons-material/CloudUpload';
import { DataGrid } from '@mui/x-data-grid';
import FormatListBulletedIcon from '@mui/icons-material/FormatListBulleted';
import Grid4x4Icon from '@mui/icons-material/Grid4x4';
import ProductCard from './ProductCard';
import ProductModal from './ProductModal';

const ProductDetail = () => {
  const [modal, setModal] = useState(false);
  const [deleteModal, setDeleteModal] = useState(false);
  const [editModal, setEditModal] = useState(false);
  const [modelContent, setModalContent] = useState([]);
  const [deleteModelContent, setDeleteModelContent] = useState([]);
  const [editModelContent, setEditModelContent] = useState([]);
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(5);
  const [image, setImage] = useState('');
  const [uploaded, isUploaded] = useState(false);
  const [searchProduct, setSearchProduct] = useState('');
  const [productCard, setProductCard] = useState(false);

  const handleImageChange = e => {
    if (e.target.files && e.target.files[0]) {
      let reader = new FileReader();

      reader.onload = function (e) {
        setImage(e.target.result);
        isUploaded(true);
      };
      reader.readAsDataURL(e.target.files[0]);
    }
  };
  const columns = [
    {
      field: 'id',
      headerName: 'ID',
      width: 30,
      headerClassName: 'super-app-theme--header',
    },
    {
      field: 'brand',
      headerName: 'Brand',
      width: 150,
      headerClassName: 'super-app-theme--header',
    },
    {
      field: 'category',
      headerName: 'Category',
      width: 150,
      headerClassName: 'super-app-theme--header',
    },
    {
      field: 'vendor',
      headerName: 'Vendor',
      width: 150,
      headerClassName: 'super-app-theme--header',
    },
    {
      field: 'product_name',
      headerName: 'Product Name',
      width: 200,
      headerClassName: 'super-app-theme--header',
    },
    {
      field: 'product_color',
      headerName: 'Product Color',
      width: 130,
      headerClassName: 'super-app-theme--header',
    },
    {
      field: 'product_size',
      headerName: 'Product Size',
      width: 120,
      headerClassName: 'super-app-theme--header',
    },
    {
      field: 'product_quantity',
      headerName: 'Product Quantity',
      width: 120,
      headerClassName: 'super-app-theme--header',
    },
    // { field: 'mark_price', headerName: 'Mark Price', width: 90 },
    // { field: 'cost_price', headerName: 'Cost Price', width: 90 },
    // { field: 'sell_price', headerName: 'Sell Price', width: 90 },
    {
      field: 'actions',
      headerName: 'Actions',
      headerClassName: 'super-app-theme--header',
      width: 171,
      renderCell: params => {
        return (
          <Box>
            <IconButton color='success'>
              <EditIcon onClick={() => handleEditModalOpen(params.row)} />
            </IconButton>
            <IconButton
              onClick={() => handleDeleteModalOpen(params.row)}
              color='error'
            >
              <DeleteIcon />
            </IconButton>
            <IconButton
              onClick={() => handleModalOpen(params.row)}
              color='info'
            >
              <InfoIcon />
            </IconButton>
          </Box>
        );
      },
    },
  ];

  const handleChangeRowsPerPage = event => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };
  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };
  const handleModalOpen = el => {
    return setModal(true), setModalContent([el]);
  };
  const handleModalClose = () => {
    return setModal(false), setModalContent([]);
  };
  const handleDeleteModalOpen = el => {
    return setDeleteModal(true), setDeleteModelContent([el]);
  };
  const handleDeleteModalClose = () => {
    return setDeleteModal(false), setDeleteModelContent([]);
  };
  const handleEditModalOpen = el => {
    return setEditModal(true), setEditModelContent([el]);
  };
  const handleEditModalClose = () => {
    return setEditModal(false), setEditModelContent([]);
  };
  const handleSearch = e => {
    setSearchProduct(e.target.value);
  };
  const handleProductCard = () => {
    setProductCard(!productCard);
  };

  return (
    <Box>
      <Paper elevation={5} sx={{ display: 'flex', alignItems: 'center', p: 1 }}>
        <Box>
          <Autocomplete
            disablePortal
            id='combo-product-box'
            getOptionLabel={el => el.product_name}
            options={ProductData}
            sx={{ width: 300 }}
            onInputChange={(e, n) => setSearchProduct(n)}
            renderInput={params => (
              <TextField
                {...params}
                onChange={handleSearch}
                color='secondary'
                label='Search Product'
              />
            )}
          />
        </Box>
        <Box
          sx={{
            flexGrow: 1,
            display: 'flex',
            justifyContent: 'flex-end',
          }}
        >
          {productCard ? (
            <IconButton>
              <FormatListBulletedIcon onClick={handleProductCard} />
            </IconButton>
          ) : (
            <IconButton onClick={handleProductCard}>
              <Grid4x4Icon />
            </IconButton>
          )}
        </Box>
      </Paper>
      {productCard ? (
        <Grid container>
          {ProductData.map(el => {
            {
              return searchProduct === '' ? (
                <Grid
                  sx={{
                    display: 'flex',
                    justifyContent: 'space-evenly',
                  }}
                  item
                  xs={12}
                  sm={5}
                  md={2.4}
                >
                  <ProductCard
                    el={el}
                    handleEditModalOpen={handleEditModalOpen}
                    handleDeleteModalOpen={handleDeleteModalOpen}
                    handleModalOpen={handleModalOpen}
                  />
                </Grid>
              ) : el.product_name.includes(searchProduct) ? (
                <Grid item xs={3}>
                  <ProductCard
                    el={el}
                    handleEditModalOpen={handleEditModalOpen}
                    handleDeleteModalOpen={handleDeleteModalOpen}
                    handleModalOpen={handleModalOpen}
                  />
                </Grid>
              ) : null;
            }
          })}
        </Grid>
      ) : (
        <Paper
          elevation={5}
          sx={{
            height: '450px',
            width: '100%',
            mt: 2,
            '& .super-app-theme--header': {
              backgroundColor: 'lightgrey',
            },
          }}
        >
          <DataGrid
            pageSize={5}
            rowsPerPageOptions={[5, 10, 15, 20]}
            rows={
              searchProduct === ''
                ? ProductData.map(el => el)
                : ProductData.filter(el =>
                    el.product_name.includes(searchProduct)
                  )
            }
            columns={columns}
          />
        </Paper>
      )}
      <ProductModal
        openModal={editModal}
        width={'70%'}
        closeModal={handleEditModalClose}
      >
        {editModelContent.map(el => {
          return (
            <>
              <Typography variant='h5'>Edit Product Details</Typography>
              <Divider sx={{ mt: 1, mb: 1 }} />
              <Grid container>
                <Grid xs={12} item>
                  <Box
                    sx={{
                      display: 'flex',
                      gap: 3,
                    }}
                  >
                    <FormControl fullWidth>
                      <InputLabel
                        color='secondary'
                        id='demo-simple-select-label'
                      >
                        Brand
                      </InputLabel>
                      <Select
                        color='secondary'
                        labelId='demo-simple-select-label'
                        id='demo-simple-select'
                        defaultValue={el.brand}
                        label='Brand'
                      >
                        <MenuItem value={el.brand}>{el.brand}</MenuItem>
                        <MenuItem value={20}>Brand 2</MenuItem>
                        <MenuItem value={30}>Brand 3</MenuItem>
                      </Select>
                    </FormControl>
                    <FormControl fullWidth>
                      <InputLabel
                        color='secondary'
                        id='demo-simple-select-label'
                      >
                        Category
                      </InputLabel>
                      <Select
                        color='secondary'
                        labelId='demo-simple-select-label'
                        id='demo-simple-select'
                        defaultValue={el.category}
                        label='Category '
                      >
                        <MenuItem value={el.category}>{el.category}</MenuItem>
                        <MenuItem value={20}>Cat 2</MenuItem>
                        <MenuItem value={30}>Cat 3</MenuItem>
                      </Select>
                    </FormControl>
                    <FormControl fullWidth>
                      <InputLabel
                        color='secondary'
                        id='demo-simple-select-label'
                      >
                        Vendor
                      </InputLabel>
                      <Select
                        color='secondary'
                        labelId='demo-simple-select-label'
                        id='demo-simple-select'
                        defaultValue={el.vendor}
                        label=' Vendor '
                      >
                        <MenuItem value={el.vendor}>{el.vendor}</MenuItem>
                        <MenuItem value={20}>ven 2</MenuItem>
                        <MenuItem value={30}>ven 3</MenuItem>
                      </Select>
                    </FormControl>
                  </Box>
                </Grid>
                <Grid xs={12} item>
                  <Grid sx={{ pt: 2 }} container>
                    <Grid
                      sx={{
                        display: 'flex',
                        flexDirection: 'column',
                        gap: 2,
                      }}
                      item
                      xs={5}
                    >
                      <Box sx={{ display: 'flex', gap: 2 }}>
                        <TextField
                          fullWidth
                          color='secondary'
                          label='Product Name'
                          value={el.product_name}
                        />
                      </Box>
                      <Box sx={{ display: 'flex', gap: 2 }}>
                        <TextField
                          color='secondary'
                          value={el.product_color}
                          label='Product Color'
                        />
                        <TextField
                          color='secondary'
                          value={el.product_size}
                          label='Product Size'
                        />
                      </Box>
                      <Box sx={{ display: 'flex', gap: 2 }}>
                        <TextField
                          color='secondary'
                          label='Product Quantity'
                          value={el.product_quantity}
                        />
                        <TextField
                          color='secondary'
                          value={el.mark_price}
                          label='Mark Price'
                        />
                      </Box>
                    </Grid>
                    <Grid item xs={1} />
                    <Grid item xs={6}>
                      <Paper
                        elevation={5}
                        sx={{
                          borderRadius: 1,
                          border: '1px solid rgba(0, 0, 0, .2)',
                          height: '100%',
                          display: 'flex',
                          flexDirection: 'row',
                          alignItems: 'center',
                          justifyContent: 'space-evenly',
                        }}
                      >
                        <Box
                          sx={{
                            padding: 3,
                            display: 'flex',
                            flexDirection: 'column',
                            alignItems: 'center',
                            justifyContent: 'center',
                          }}
                        >
                          <CloudUploadIcon sx={{ fontSize: '100px' }} />
                          <Button
                            onChange={handleImageChange}
                            component='label'
                            variant='outlined'
                            color='secondary'
                          >
                            Product Image
                            <input type='file' hidden />
                          </Button>
                        </Box>
                        <Box
                          sx={{
                            // flexGrow: 1,
                            display: 'flex',
                            alignItems: 'center',
                            justifyContent: 'center',
                          }}
                        >
                          {uploaded ? (
                            <img
                              style={{
                                height: '160px',
                                width: '160px',
                              }}
                              src={image}
                              alt='Brand Picture'
                            />
                          ) : (
                            <img
                              style={{
                                height: '160px',
                                width: '160px',
                              }}
                              src={unknown_profile}
                              alt='Unknown Image'
                            />
                          )}
                        </Box>
                      </Paper>
                    </Grid>
                    <Grid item sx={{ mt: 2, display: 'flex', gap: 9 }} xs={12}>
                      <Box sx={{ width: '42%', display: 'flex', gap: 2 }}>
                        <TextField
                          color='secondary'
                          value={el.cost_price}
                          label='Cost Price'
                        />
                        <TextField
                          color='secondary'
                          value={el.sell_price}
                          label='Sell Size'
                        />
                      </Box>
                      <Box
                        sx={{
                          display: 'flex',
                          justifyContent: 'flex-end',
                          alignItems: 'flex-end',
                          gap: 2,
                          flexGrow: 1,
                        }}
                      >
                        <Button variant='outlined' size='large' color='error'>
                          Clear
                        </Button>
                        <Button variant='contained' size='large' color='info'>
                          Update
                        </Button>
                      </Box>
                    </Grid>
                  </Grid>
                </Grid>
              </Grid>
            </>
          );
        })}
      </ProductModal>

      <ProductModal
        width={'70%'}
        openModal={modal}
        closeModal={handleModalClose}
      >
        {modelContent.map(el => {
          return (
            <>
              <Typography variant='h5'>Product Details</Typography>
              <Divider sx={{ mt: 1, mb: 1 }} />
              <Box sx={{ display: 'flex' }}>
                <img src={unknown_profile} height='330' width='300' />
                <Box sx={{ flexGrow: 1, ml: 10 }}>
                  <Box
                    sx={{ display: 'flex', justifyContent: 'space-between' }}
                  >
                    <Typography sx={{ fontWeight: 'bold' }}>
                      Brand : {el.brand}
                    </Typography>
                    <Typography sx={{ fontWeight: 'bold' }}>
                      Category : {el.category}
                    </Typography>
                    <Typography sx={{ fontWeight: 'bold' }}>
                      Vendor : {el.vendor}
                    </Typography>
                  </Box>
                  <Divider sx={{ mt: 1, mb: 1 }} />
                  <Typography sx={{ fontWeight: 'bold' }}>
                    Product Name : {el.product_name}
                  </Typography>
                  <Divider sx={{ mt: 1, mb: 1 }} />
                  <Typography sx={{ fontWeight: 'bold' }}>
                    Product Color : {el.product_color}
                  </Typography>
                  <Divider sx={{ mt: 1, mb: 1 }} />
                  <Typography sx={{ fontWeight: 'bold' }}>
                    Product Size : {el.product_size}
                  </Typography>
                  <Divider sx={{ mt: 1, mb: 1 }} />
                  <Typography sx={{ fontWeight: 'bold' }}>
                    Product Quantity : {el.product_quantity}
                  </Typography>
                  <Divider sx={{ mt: 1, mb: 1 }} />
                  <Typography sx={{ fontWeight: 'bold' }}>
                    Mark Price : Rs. {el.mark_price}
                  </Typography>
                  <Divider sx={{ mt: 1, mb: 1 }} />
                  <Typography sx={{ fontWeight: 'bold' }}>
                    Cost Price : Rs. {el.cost_price}
                  </Typography>
                  <Divider sx={{ mt: 1, mb: 1 }} />
                  <Typography sx={{ fontWeight: 'bold' }}>
                    Sell Price : Rs. {el.sell_price}
                  </Typography>
                  <Divider sx={{ mt: 1 }} />
                </Box>
              </Box>
            </>
          );
        })}
      </ProductModal>
      <ProductModal openModal={deleteModal} closeModal={handleDeleteModalClose}>
        {deleteModelContent.map(el => {
          return (
            <Box
              sx={{
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
              }}
            >
              <Typography>
                Are you sure you want to delete &nbsp;
                <span style={{ color: 'red', fontWeight: '700' }}>
                  {el.product_name}
                </span>
                ?
              </Typography>
              <Box
                sx={{
                  display: 'flex',
                  mt: 4,
                  alignItems: 'center',
                  gap: 4,
                }}
              >
                <Button sx={{ width: '100px' }} color='info' variant='outlined'>
                  No
                </Button>
                <Button
                  sx={{ width: '100px' }}
                  color='error'
                  variant='outlined'
                >
                  Yes
                </Button>
              </Box>
            </Box>
          );
        })}
      </ProductModal>
    </Box>
  );
};

export default ProductDetail;
