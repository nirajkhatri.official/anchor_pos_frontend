import {
  Button,
  Card,
  CardActions,
  CardContent,
  CardMedia,
  Paper,
  Typography,
} from '@mui/material';
import { Box } from '@mui/system';
import React from 'react';
import unknown_profile from '../../Images/unknown_profile.png';

const ProductCard = ({
  el,
  handleEditModalOpen,
  handleDeleteModalOpen,
  handleModalOpen,
}) => {
  return (
    <Paper sx={{ maxWidth: '250px', mt: 2 }} elevation={5}>
      <Card sx={{ maxWidth: '250px' }}>
        <CardMedia>
          <img
            src='https://picsum.photos/200/300'
            style={{ height: '150px', width: '100%' }}
          />
        </CardMedia>
        <CardContent
          sx={{
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
          }}
        >
          <Typography variant='subtitle2' sx={{ fontWeight: 'bold' }}>
            Product : {el.product_name}
          </Typography>
          <Typography variant='subtitle2'>Brand : {el.brand}</Typography>
        </CardContent>
        <CardActions>
          <Box
            sx={{
              display: 'flex',
              width: '100%',
              justifyContent: 'space-evenly',
            }}
          >
            <Button
              onClick={() => handleEditModalOpen(el)}
              variant='outlined'
              color='success'
              size='small'
            >
              Edit
            </Button>
            <Button
              onClick={() => handleDeleteModalOpen(el)}
              variant='outlined'
              color='error'
              size='small'
            >
              Delete
            </Button>
            <Button
              onClick={() => handleModalOpen(el)}
              variant='outlined'
              color='info'
              size='small'
            >
              Info
            </Button>
          </Box>
        </CardActions>
      </Card>
    </Paper>
  );
};

export default ProductCard;
