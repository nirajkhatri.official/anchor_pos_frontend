import { IconButton, Modal, Paper } from '@mui/material';
import React from 'react';
import CloseIcon from '@mui/icons-material/Close';

const ProductModal = ({ children, openModal, closeModal, width }) => {
  return (
    <Modal
      open={openModal}
      onClose={closeModal}
      aria-labelledby='modal-modal-title'
      aria-describedby='modal-modal-description'
    >
      <Paper
        elevation={5}
        sx={{
          position: 'absolute',
          top: '50%',
          left: '50%',
          transform: 'translate(-50%, -50%)',
          width: `${width}`,
          bgcolor: 'background.paper',
          boxShadow: 24,
          p: 4,
        }}
      >
        <IconButton
          onClick={closeModal}
          sx={{
            position: 'absolute',
            top: -10,
            backgroundColor: 'secondary.main',
            color: 'primary.main',
            right: -10,
            ':hover': {
              backgroundColor: 'secondary.main',
              color: 'primary.main',
            },
          }}
        >
          <CloseIcon />
        </IconButton>
        {children}
      </Paper>
    </Modal>
  );
};

export default ProductModal;
