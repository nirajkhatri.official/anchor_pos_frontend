import {
  Button,
  FormControl,
  Grid,
  IconButton,
  InputLabel,
  MenuItem,
  Modal,
  Paper,
  Select,
  TextField,
  Typography,
} from '@mui/material';
import React, { useState } from 'react';
import { Box } from '@mui/system';
import AddCircleRoundedIcon from '@mui/icons-material/AddCircleRounded';
import CloudUploadIcon from '@mui/icons-material/CloudUpload';
import unknown_profile from '../../Images/unknown_profile.png';
import CloseIcon from '@mui/icons-material/Close';

const AddProduct = () => {
  const [modal, setModal] = useState(false);

  const handleOpen = () => setModal(true);
  const handleClose = () => setModal(false);
  const [image, setImage] = useState('');
  const [uploaded, isUploaded] = useState(false);
  const handleImageChange = e => {
    if (e.target.files && e.target.files[0]) {
      let reader = new FileReader();

      reader.onload = function (e) {
        setImage(e.target.result);
        isUploaded(true);
      };
      reader.readAsDataURL(e.target.files[0]);
    }
  };
  return (
    <>
      <Paper elevation={5}>
        <Box sx={{ padding: 3, display: 'flex', alignItems: 'center' }}>
          <IconButton>
            <AddCircleRoundedIcon fontSize='medium' color='success' />
          </IconButton>
          <Typography variant='h5'>Add Product</Typography>
        </Box>
        <Grid sx={{ padding: 3 }} rowSpacing={3} container>
          <Grid
            sx={{
              display: 'flex',
              flexDirection: 'column',
              gap: 2,
            }}
            item
            xs={12}
          >
            <Box sx={{ display: 'flex', gap: 3 }}>
              <FormControl fullWidth>
                <InputLabel color='secondary' id='demo-simple-select-label'>
                  Brand
                </InputLabel>
                <Select
                  color='secondary'
                  labelId='demo-simple-select-label'
                  id='demo-simple-select'
                  //   value={age}
                  label='Brand'
                  // onChange={handleChange}
                >
                  <MenuItem value={10}>Brand 1</MenuItem>
                  <MenuItem value={20}>Brand 2</MenuItem>
                  <MenuItem value={30}>Brand 3</MenuItem>
                </Select>
              </FormControl>
              <FormControl fullWidth>
                <InputLabel color='secondary' id='demo-simple-select-label'>
                  Category
                </InputLabel>
                <Select
                  color='secondary'
                  labelId='demo-simple-select-label'
                  id='demo-simple-select'
                  // value={age}
                  label='Category '
                  // onChange={handleChange}
                >
                  <MenuItem value={10}>Cat 1</MenuItem>
                  <MenuItem value={20}>Cat 2</MenuItem>
                  <MenuItem value={30}>Cat 3</MenuItem>
                </Select>
              </FormControl>
              <FormControl fullWidth>
                <InputLabel color='secondary' id='demo-simple-select-label'>
                  Vendor
                </InputLabel>
                <Select
                  color='secondary'
                  labelId='demo-simple-select-label'
                  id='demo-simple-select'
                  // value={age}
                  label=' Vendor '
                  // onChange={handleChange}
                >
                  <MenuItem value={10}>Ven 1</MenuItem>
                  <MenuItem value={20}>ven 2</MenuItem>
                  <MenuItem value={30}>ven 3</MenuItem>
                </Select>
              </FormControl>
            </Box>
          </Grid>

          <Grid item xs={12}>
            <Button
              onClick={handleOpen}
              variant='contained'
              size='large'
              color='info'
            >
              <IconButton>
                <AddCircleRoundedIcon color='error' />
              </IconButton>
              Add New Product
            </Button>
          </Grid>
        </Grid>
        <Modal
          open={modal}
          onClose={handleClose}
          aria-labelledby='modal-modal-title'
          aria-describedby='modal-modal-description'
        >
          <Paper
            sx={{
              position: 'absolute',
              top: '50%',
              left: '50%',
              transform: 'translate(-50%, -50%)',
              width: '1000px',
              bgcolor: 'primary.main',
              boxShadow: 24,
              p: 2,
            }}
          >
            <IconButton
              onClick={handleClose}
              sx={{
                position: 'absolute',
                top: -10,
                backgroundColor: 'secondary.main',
                color: 'primary.main',
                right: -10,
                ':hover': {
                  backgroundColor: 'secondary.main',
                  color: 'primary.main',
                },
              }}
            >
              <CloseIcon />
            </IconButton>
            <Paper elevation={5}>
              <Grid sx={{ padding: 3 }} container>
                <Grid
                  sx={{
                    display: 'flex',
                    flexDirection: 'column',
                    gap: 2,
                  }}
                  item
                  xs={5}
                >
                  <Box sx={{ display: 'flex', gap: 2 }}>
                    <TextField
                      fullWidth
                      color='secondary'
                      label='Product Name'
                    />
                  </Box>
                  <Box sx={{ display: 'flex', gap: 2 }}>
                    <TextField color='secondary' label='Product Color' />
                    <TextField color='secondary' label='Product Size' />
                  </Box>
                  <Box sx={{ display: 'flex', gap: 2 }}>
                    <TextField color='secondary' label='Product Quantity' />
                    <TextField color='secondary' label='Mark Price' />
                  </Box>
                </Grid>
                <Grid item xs={1} />
                <Grid item xs={6}>
                  <Paper
                    elevation={5}
                    sx={{
                      borderRadius: 1,
                      border: '1px solid rgba(0, 0, 0, .2)',
                      height: '100%',
                      display: 'flex',
                      flexDirection: 'row',
                      alignItems: 'center',
                      justifyContent: 'space-evenly',
                    }}
                  >
                    <Box
                      sx={{
                        padding: 3,
                        display: 'flex',
                        flexDirection: 'column',
                        alignItems: 'center',
                        justifyContent: 'center',
                      }}
                    >
                      <CloudUploadIcon sx={{ fontSize: '100px' }} />
                      <Button
                        onChange={handleImageChange}
                        component='label'
                        variant='outlined'
                        color='secondary'
                      >
                        Product Image
                        <input type='file' hidden />
                      </Button>
                    </Box>
                    <Box
                      sx={{
                        // flexGrow: 1,
                        display: 'flex',
                        alignItems: 'center',
                        justifyContent: 'center',
                      }}
                    >
                      {uploaded ? (
                        <img
                          style={{ height: '160px', width: '160px' }}
                          src={image}
                          alt='Brand Picture'
                        />
                      ) : (
                        <img
                          style={{ height: '160px', width: '160px' }}
                          src={unknown_profile}
                          alt='Unknown Image'
                        />
                      )}
                    </Box>
                  </Paper>
                </Grid>
                <Grid item sx={{ mt: 2, display: 'flex', gap: 9 }} xs={12}>
                  <Box sx={{ width: '42%', display: 'flex', gap: 2 }}>
                    <TextField color='secondary' label='Cost Price' />
                    <TextField color='secondary' label='Sell Size' />
                  </Box>
                  <Box
                    sx={{
                      display: 'flex',
                      justifyContent: 'flex-end',
                      alignItems: 'flex-end',
                      gap: 2,
                      flexGrow: 1,
                    }}
                  >
                    <Button variant='outlined' size='large' color='error'>
                      Clear
                    </Button>
                    <Button variant='contained' size='large' color='success'>
                      Submit
                    </Button>
                    <Button variant='contained' size='large' color='info'>
                      Add Another Product
                    </Button>
                  </Box>
                </Grid>
              </Grid>
            </Paper>
          </Paper>
        </Modal>
      </Paper>
    </>
  );
};

export default AddProduct;
