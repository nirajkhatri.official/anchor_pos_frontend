import { Button, Grid, Paper, TextField } from '@mui/material';
import React, { useState } from 'react';
import { Box } from '@mui/system';
import CloudUploadIcon from '@mui/icons-material/CloudUpload';
import unknown_profile from '../../Images/unknown_profile.png';

const AddEmployee = () => {
  const [image, setImage] = useState([]);
  const [uploaded, isUploaded] = useState(false);
  const [employeeImageUploaded, setEmployeeImageUploaded] = useState(false);
  const [employeeImage, setEmployeeImage] = useState('');
  const handleEmployeeImageChange = e => {
    if (e.target.files && e.target.files[0]) {
      let reader = new FileReader();

      reader.onload = function (e) {
        setEmployeeImage(e.target.result);
        setEmployeeImageUploaded(true);
      };
      reader.readAsDataURL(e.target.files[0]);
    }
  };
  const handleImageChange = e => {
    if (e.target.files) {
      const fileArray = Array.from(e.target.files).map(file =>
        URL.createObjectURL(file)
      );

      setImage(prev => prev.concat(fileArray));
      isUploaded(true);
      Array.from(e.target.files).map(file => URL.revokeObjectURL(file));
    }
  };

  return (
    <>
      <Paper elevation={5}>
        <Grid sx={{ padding: 3 }} container>
          <Grid
            sx={{
              display: 'flex',
              flexDirection: 'column',
              gap: 2,
            }}
            item
            xs={5}
          >
            <TextField color='secondary' label='Employee Name' />
            <TextField color='secondary' label='Employee Phone' />
            <TextField color='secondary' label='Employee Mail' />
            <TextField color='secondary' label='Employee Role' />
            <Box
              sx={{
                height: '100%',
              }}
            >
              <Box
                sx={{
                  display: 'flex',
                  justifyContent: 'flex-start',
                  alignItems: 'flex-end',
                  height: '100%',
                  gap: 2,
                }}
              >
                <Button variant='contained' size='large' color='error'>
                  Clear
                </Button>
                <Button variant='outlined' size='large' color='success'>
                  Submit
                </Button>
              </Box>
            </Box>
          </Grid>
          <Grid item xs={1} />
          <Grid item xs={6}>
            <Paper
              elevation={5}
              sx={{
                borderRadius: 1,
                border: '1px solid rgba(0, 0, 0, .2)',
                height: '50%',
                display: 'flex',
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-evenly',
              }}
            >
              <Box
                sx={{
                  padding: 3,
                  display: 'flex',
                  flexDirection: 'column',
                  alignItems: 'center',
                  justifyContent: 'center',
                }}
              >
                <CloudUploadIcon sx={{ fontSize: '100px' }} />
                <Button
                  onChange={handleEmployeeImageChange}
                  component='label'
                  variant='outlined'
                  color='secondary'
                >
                  Employee Image
                  <input type='file' hidden />
                </Button>
              </Box>
              <Box
                sx={{
                  // flexGrow: 1,
                  display: 'flex',
                  alignItems: 'center',
                  justifyContent: 'center',
                }}
              >
                {employeeImageUploaded ? (
                  <img
                    style={{ height: '160px', width: '160px' }}
                    src={employeeImage}
                    alt='Brand Picture'
                  />
                ) : (
                  <img
                    style={{ height: '160px', width: '160px' }}
                    src={unknown_profile}
                    alt='Unknown Image'
                  />
                )}
              </Box>
            </Paper>
            <Paper
              elevation={5}
              sx={{
                mt: 1,
                borderRadius: 1,
                border: '1px solid rgba(0, 0, 0, .2)',
                height: '50%',
                display: 'flex',
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-evenly',
              }}
            >
              <Box
                sx={{
                  padding: 3,
                  display: 'flex',
                  flexDirection: 'column',
                  alignItems: 'center',
                  justifyContent: 'center',
                }}
              >
                <CloudUploadIcon sx={{ fontSize: '100px' }} />
                <Button
                  onChange={handleImageChange}
                  component='label'
                  variant='outlined'
                  color='secondary'
                >
                  Document Image
                  <input multiple type='file' hidden />
                </Button>
              </Box>
              <Box
                sx={{
                  // flexGrow: 1,
                  display: 'flex',
                  alignItems: 'center',
                  justifyContent: 'center',
                }}
              >
                {uploaded ? (
                  <>
                    {image.map(el => (
                      <Box sx={{ ml: 2 }}>
                        <img
                          style={{ height: '160px', width: '160px' }}
                          src={el}
                          alt='Brand Picture'
                        />
                      </Box>
                    ))}
                  </>
                ) : (
                  <img
                    style={{ height: '160px', width: '160px' }}
                    src={unknown_profile}
                    alt='Unknown Image'
                  />
                )}
              </Box>
            </Paper>
          </Grid>
          {/* <Grid item sx={{ mt: 5 }} xs={12}>
            <Box
              sx={{
                display: 'flex',
                justifyContent: 'flex-start',
                gap: 2,
                mr: 3,
              }}
            >
              <Button variant='contained' size='large' color='error'>
                Clear
              </Button>
              <Button variant='outlined' size='large' color='success'>
                Submit
              </Button>
            </Box>
          </Grid> */}
        </Grid>
      </Paper>
    </>
  );
};

export default AddEmployee;
