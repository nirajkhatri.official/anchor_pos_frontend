import ReactDOM from 'react-dom';
import App from './App';
import './style.css';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import React, { createContext } from 'react';

export const ColorModeContext = createContext({
  toggleColorMode: () => {},
  mode: 'light',
});

const getDesignTokens = mode => ({
  palette: {
    mode,
    ...(mode === 'light'
      ? {
          // palette values for light mode
          primary: {
            main: '#fff',
          },
          secondary: {
            main: '#000',
          },
          // divider: ' rgba(0, 0, 0, 0.12)',
        }
      : {
          // palette values for dark mode
          primary: {
            main: '#000',
            contrastText: '#fff',
          },
          secondary: {
            main: '#fff',
            accordian: 'rgba(0,0,0,.7)',
            contrastText: '#000',
          },
          // divider: 'rgba(0, 0, 0, 0.12)',
        }),
  },
});
const Main = () => {
  const [mode, setMode] = React.useState('light');
  const colorMode = React.useMemo(
    () => ({
      // The dark mode switch would invoke this method
      toggleColorMode: () => {
        setMode(prevMode => (prevMode === 'light' ? 'dark' : 'light'));
      },
    }),
    []
  );
  // Update the theme only if the mode changes
  const theme = React.useMemo(() => createTheme(getDesignTokens(mode)), [mode]);

  return (
    <ColorModeContext.Provider value={colorMode}>
      <ThemeProvider theme={theme}>
        <App />
      </ThemeProvider>
    </ColorModeContext.Provider>
  );
};

ReactDOM.render(<Main />, document.getElementById('root'));
