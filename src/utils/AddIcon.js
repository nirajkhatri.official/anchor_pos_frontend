import { IconButton, Typography } from '@mui/material';
import { Box } from '@mui/system';
import React from 'react';
import AddCircleRoundedIcon from '@mui/icons-material/AddCircleRounded';

const AddIcon = ({ title }) => {
  return (
    <Box sx={{ padding: 3, display: 'flex', alignItems: 'center' }}>
      <IconButton>
        <AddCircleRoundedIcon fontSize='medium' color='success' />
      </IconButton>
      <Typography variant='h5'>{title}</Typography>
    </Box>
  );
};

export default AddIcon;
