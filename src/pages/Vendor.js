import { Button, Grid, Paper, TextField } from '@mui/material';
import React, { useState } from 'react';
import { Box } from '@mui/system';
import CloudUploadIcon from '@mui/icons-material/CloudUpload';
import unknown_profile from '../Images/unknown_profile.png';

const Vendor = () => {
  const [image, setImage] = useState('');
  const [uploaded, isUploaded] = useState(false);
  const handleImageChange = e => {
    if (e.target.files && e.target.files[0]) {
      let reader = new FileReader();

      reader.onload = function (e) {
        setImage(e.target.result);
        isUploaded(true);
      };
      reader.readAsDataURL(e.target.files[0]);
    }
  };

  return (
    <>
      <Paper elevation={5}>
        <Grid sx={{ padding: 3 }} container>
          <Grid
            sx={{
              display: 'flex',
              flexDirection: 'column',
              gap: 2,
            }}
            item
            xs={5}
          >
            <TextField color='secondary' label='Vendor Name' />
            <TextField color='secondary' label='Vendor Location' />
            <TextField color='secondary' label='Vendor Contact' />
            <TextField color='secondary' label='Vendor Mail' />
          </Grid>
          <Grid item xs={1} />
          <Grid item xs={6}>
            <Paper
              elevation={5}
              sx={{
                borderRadius: 1,
                border: '1px solid rgba(0, 0, 0, .2)',
                height: '100%',
                display: 'flex',
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-evenly',
              }}
            >
              <Box
                sx={{
                  padding: 3,
                  display: 'flex',
                  flexDirection: 'column',
                  alignItems: 'center',
                  justifyContent: 'center',
                }}
              >
                <CloudUploadIcon sx={{ fontSize: '100px' }} />
                <Button
                  onChange={handleImageChange}
                  component='label'
                  variant='outlined'
                  color='secondary'
                >
                  Upload Image
                  <input type='file' hidden />
                </Button>
              </Box>
              <Box
                sx={{
                  // flexGrow: 1,
                  display: 'flex',
                  alignItems: 'center',
                  justifyContent: 'center',
                }}
              >
                {uploaded ? (
                  <img
                    style={{ height: '160px', width: '160px' }}
                    src={image}
                    alt='Brand Picture'
                  />
                ) : (
                  <img
                    style={{ height: '160px', width: '160px' }}
                    src={unknown_profile}
                    alt='Unknown Image'
                  />
                )}
              </Box>
            </Paper>
          </Grid>
          <Grid sx={{ mt: 2 }} container>
            <Grid item xs={5}>
              <TextField fullWidth color='secondary' label='Vendor Mail' />
            </Grid>
            <Grid item xs={1} />
            <Grid item xs={6}>
              <Box
                sx={{
                  display: 'flex',
                  justifyContent: 'flex-end',
                  alignItems: 'flex-end',
                  height: '100%',
                  gap: 2,
                }}
              >
                <Button variant='contained' size='large' color='error'>
                  Clear
                </Button>
                <Button variant='outlined' size='large' color='success'>
                  Submit
                </Button>
              </Box>
            </Grid>
          </Grid>
        </Grid>
      </Paper>
    </>
  );
};

export default Vendor;
