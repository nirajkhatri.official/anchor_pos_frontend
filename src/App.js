import React from 'react';
import Header from './components/Header';
import Dashboard from './pages/Dashboard';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import { Box } from '@mui/system';
import Error from './pages/Error';
import GiftCard from './pages/GiftCard';
import Category from './pages/Category';
import Vendor from './pages/Vendor';
import Brand from './pages/Brand';
import Report from './pages/Report';
import Product from './pages/Product';
import Sale from './pages/Sale';
import Employee from './pages/Employee';
import AddProduct from './components/Product/AddProduct';
import Customer from './components/Customer/AddCustomer';
import Note from './pages/Note';
import Notification from './components/Notification/index';
import ProductDetail from './components/Product/ProductDetail';
import Order from './components/Order';

const App = () => {
  return (
    <BrowserRouter>
      <Header>
        <Routes>
          <Route path='/' element={<Dashboard />} />
          <Route path='/category' element={<Category />} />
          <Route path='/vendor' element={<Vendor />} />
          <Route path='/brand' element={<Brand />} />
          <Route path='/report' element={<Report />} />
          <Route path='/product' element={<Product />} />
          <Route path='/order' element={<Order />} />
          <Route path='/sale' element={<Sale />} />
          <Route path='/employee' element={<Employee />} />
          <Route path='/customer' element={<Customer />} />
          <Route path='/giftcard' element={<GiftCard />} />
          <Route path='/addproduct' element={<AddProduct />} />
          <Route path='/productdetail' element={<ProductDetail />} />
          <Route path='/notification' element={<Notification />} />
          <Route path='/note' element={<Note />} />
          <Route path='*' element={<Error />} />
        </Routes>
      </Header>
    </BrowserRouter>
  );
};

export default App;
